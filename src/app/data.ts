import { Produit } from "./produit";

export const tabProduits: Array<Produit> = [
  new Produit("Produit 1","Fournisseur 1","C'est la description 1 de notre produit 1 alors s'il vous plait acheter ce produit :)",10,"fournisseur1@gmail.com",["Ingrédient 1","Ingrédient 2"],"Condition de conservation 1",10.5),
  new Produit("Produit 2","Fournisseur 2","Description 2",15,"fournisseur2@gmail.com",["Ingrédient 3","Ingrédient 2"],"Condition de conservation 2",100.5),
  new Produit("Produit 3","Fournisseur 3","Description 2",30,"fournisseur3@gmail.com",["Ingrédient 1","Ingrédient 3","Ingrédient 2"],"Condition de conservation 3",50.65),
  new Produit("Produit 4","Fournisseur 4","Description 2",5,"fournisseur4@gmail.com",["Ingrédient 3","Ingrédient 1","Ingrédient 2"],"Condition de conservation 3",1700.5),
  new Produit("Produit 5","Fournisseur 5","Description 2",60,"fournisseur5@gmail.com",["Ingrédient 3","Ingrédient 1","Ingrédient 2","Ingrédient 4"],"Condition de conservation 3",500.5)
];
