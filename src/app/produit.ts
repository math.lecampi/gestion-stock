export class Produit {
    nom: string;
    fournisseur: string;
    email_fournisseur: string;
    list_ingredients: string[];
    description: string;
    age: number;
    condition_conservation: string;
    prix: number;

    constructor(pNom:string,pFournisseur:string,pDescription:string,pAge:number,pEmailFournisseur?:string,pListIngredients?:string[],pConditionConservation?:string,pPrix?:number) {
        this.nom = pNom;
        this.fournisseur = pFournisseur;
        this.email_fournisseur = pEmailFournisseur;
        this.list_ingredients = pListIngredients;
        this.description = pDescription;
        this.age = pAge;
        this.condition_conservation = pConditionConservation;
        this.prix = pPrix
      }
  }