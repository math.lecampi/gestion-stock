import { Component, OnInit } from '@angular/core';
import { Produit } from './produit';
import { ProduitService } from './produit.service';
import { FormBuilder, FormGroup, FormControl, Validators } from'@angular/forms';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'gestion-stock';
  tabProduits:Produit[];

  nomCtrl: FormControl;
  fournisseurCtrl: FormControl;
  descriptionCtrl: FormControl;
  ageCtrl: FormControl;
  produitForm: FormGroup;

  formIsHidden = true;

  constructor(public produitService: ProduitService, fb: FormBuilder) { 
    this.nomCtrl = fb.control('',[Validators.required]);
    this.fournisseurCtrl = fb.control('',[Validators.required]);
    this.descriptionCtrl = fb.control('',[Validators.required]);
    this.ageCtrl = fb.control('',[Validators.required,Validators.pattern("[0-9]+")]);

    this.produitForm = fb.group({
      nom: this.nomCtrl,
      fournisseur: this.fournisseurCtrl,
      description: this.descriptionCtrl,
      age: this.ageCtrl
    });
  }

  ngOnInit() {
    this.tabProduits = this.produitService.list();
  }

  addProduit(){
    let produit:Produit = new Produit(this.produitForm.value.nom,this.produitForm.value.fournisseur,this.produitForm.value.description,this.produitForm.value.age);
    this.produitService.add(produit);
    this.formIsHidden = true;
  }

  reset() {
    this.nomCtrl.setValue('');
    this.fournisseurCtrl.setValue('');
    this.ageCtrl.setValue('');
    this.descriptionCtrl.setValue('');
  }

  afficheFormulaire(){
    this.reset();
    this.formIsHidden = false;
  }

  deleteProduit(produit){
    const index: number = this.tabProduits.indexOf(produit);
    if (index !== -1) {
      this.tabProduits.splice(index, 1);
    }
  }
}
