import { Injectable } from '@angular/core';
import { tabProduits } from './data';
import { Produit } from './produit';

@Injectable({
  providedIn: 'root'
})
export class ProduitService {

  constructor() { 
  }

  list(){
    return tabProduits;
  }
  add(produit:Produit){
    tabProduits.push(produit)
  }
}
